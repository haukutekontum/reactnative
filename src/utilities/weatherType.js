export const weatherType = {
    Thunderstorm: {
        icon: 'zap',
        message: 'It could get noisy',
        backgroundColor: '#000000'
    },
    Drizzle: {
        icon: 'cloud-rain',
        message: 'It might rain a little',
        backgroundColor: '#36454f'
    },
    Rain: {
        icon: 'umbrella',
        message: 'You will need an umbrella',
        backgroundColor: '#0000FF'
    },
    Snow: {
        icon: 'cloud-snow',
        message: 'Let build a snowman',
        backgroundColor: '#7F6065'
    },
    Clear: {
        icon: 'sun',
        message: 'Its perfect t-shirt weather',
        backgroundColor: '#I47200'
    },
    Clouds: {
        icon: 'cloud',
        message: 'You could lift in the cloud',
        backgroundColor: '#363636'
    },
    Haze: {
        icon: 'wind',
        message: 'It might be a bit damp',
        backgroundColor: '#585A6E'
    },
    Mist: {
        icon: 'align-justify',
        message: 'It might be hard to see',
        backgroundColor: '#3e3e37'
    },
}
